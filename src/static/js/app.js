'use strict';

function copy_properties(source, target) {
    for (var prop in source) {
        if (source.hasOwnProperty(prop)) {
            if (source[prop] != null) {
                target[prop] = source[prop];
            }
        }
    }
}

function Scenario(obj) { copy_properties(obj, this); }
function ScenarioGenerator(obj) { copy_properties(obj, this); }
function WaveformGenerator(obj) { copy_properties(obj, this); }
function RandomStationGenerator(obj) { copy_properties(obj, this); }
function SatelliteSceneGenerator(obj) { copy_properties(obj, this); }
function DCSourceGenerator(obj) { copy_properties(obj, this); }
function WhiteNoiseGenerator(obj) { copy_properties(obj, this); }
function ScenarioCollectionItem(obj) { copy_properties(obj, this); }

var yaml_type_map = [
    ['!pf.scenario.Scenario', Scenario],
    ['!pf.scenario.ScenarioGenerator', ScenarioGenerator],
    ['!pf.scenario.WaveformGenerator', WaveformGenerator],
    ['!pf.scenario.RandomStationGenerator', RandomStationGenerator],
    ['!pf.scenario.SatelliteSceneGenerator', SatelliteSceneGenerator],
    ['!pf.scenario.DCSourceGenerator', DCSourceGenerator],
    ['!pf.scenario.WhiteNoiseGenerator', WhiteNoiseGenerator],
    ['!pf.scenario.ScenarioCollectionItem', ScenarioCollectionItem]
];

function make_constructor(type) {
    var type = type;
    var construct = function(data) {
        return new type(data);
    };
    return construct;
}

var yaml_types = [];
for (var i=0; i<yaml_type_map.length; i++) {
    var type = yaml_type_map[i][1]
    var t = new jsyaml.Type(yaml_type_map[i][0], {
        kind: 'mapping',
        instanceOf: type,
        construct: make_constructor(type),
    });
    yaml_types.push(t);
}

function parse_fields_float(fields, input, output, error, factor) {
    parse_fields(fields, input, output, error, factor, parseFloat);
}

function parse_fields_int(fields, input, output, error) {
    parse_fields(fields, input, output, error, 1.0, parseInt);
}

function parse_fields(fields, input, output, error, factor, parse) {
    for (var i=0; i<fields.length; i++) {
        var field = fields[i];
        if (input[field].length == 0) {
            val = null;
        } else {
            var val = parse(input[field]) * factor;
            if (val.isNaN) {
                error[field] = true;
                return false;
            }
        }
        output[field] = val;
    }
}

var scenario_schema = jsyaml.Schema.create(yaml_types);

angular.module('scenarioApp', ['ngRoute'])

    .controller('NavigationController', function($scope, $route, $routeParams, $location) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;

        $scope.active = function(path) {
            return (path === $location.path().substr(0,path.length)) ? 'active' : '';
        };
    })

    .config(function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/home', {
                templateUrl: 'static/home.html',
            })
            .when('/scenarios', {
                templateUrl: 'static/scenarios.html',
            })
            .when('/gt', {
                templateUrl: 'static/gt.html',
            })
            .when('/documentation', {
                templateUrl: 'static/documentation.html',
            })
            .when('/contact', {
                templateUrl: 'static/contact.html',
            })
            .otherwise({
                redirectTo: '/scenarios',
            });
    })

    .factory('Scenarios', function($http, $location) {
        var scenarios = [];
        var funcs = {};
        var error = null;
        var current_scenario_id = null;

        var stores = [];

        funcs.receiveGFStores = function(response) {
            stores.length = 0;
            for (var i=0; i<response.data.length; i++) {
                var store = response.data[i];
                store.suitable = true;
                stores.push(store);
            };
        };

        funcs.receiveSuitableGFStoreIDs = function(response) {
            var suitable = new Set(response.data);
            for (var i=0; i<stores.length; i++) {
                stores[i].suitable = suitable.has(stores[i].id);
            }
        };

        funcs.queryGFStores = function() {
            $http.get('/gf_stores/list').then(funcs.receiveGFStores);
        };

        funcs.querySuitableGFStores = function(params) {
            $http.get('/gf_stores/suitable', {params: params}).then(funcs.receiveSuitableGFStoreIDs);
        };

        funcs.receiveScenarios = function(response) {
            scenarios.length = 0;
            jsyaml.safeLoadAll(
                response.data,
                function(doc) { scenarios.push(doc); },
                {schema: scenario_schema});
        };

        funcs.queryScenarios = function() {
            $http.get('/scenarios/').then(funcs.receiveScenarios);
        };

        funcs.generateScenario = function(sc) {

            /*
            var s = '--- !grond.scenario.ScenarioGenerator\navoid_water: true\nradius: 1000000.0\nntries: 500\nstation_generator: !grond.scenario.RandomStationGenerator\n  avoid_water: true\n  ntries: 500\n  nstations: 10\nsatellite_generator: !grond.scenario.SatelliteSceneGenerator\n  avoid_water: true\n  ntries: 500\n  inclination: 98.2\n  apogee: 693000.0\n  swath_width: 250000.0\n  track_length: 150000.0\n  incident_angle: 29.1\n  resolution: [250, 250]\n  mask_water: true\nsource_generator: !grond.scenario.DCSourceGenerator\n  radius: 100000.0\n  ntries: 500\n  avoid_water: false\n  nevents: 10\n  time_min: 2017-01-01 00:00:00\n  time_max: 2017-01-02 00:00:00\n  magnitude_min: 4.0\n  magnitude_max: 7.0\n  depth_min: 0.0\n  depth_max: 30000.0\nnoise_generator: !grond.scenario.WhiteNoiseGenerator\n  scale: 1.0e-06\nvmin_cut: 2000.0\nvmax_cut: 8000.0\nfmin: 0.01';
            var sc = jsyaml.safeLoad(s, {schema: scenario_schema})
            console.log(sc);
            console.log(jsyaml.safeDump(sc, {schema: scenario_schema}));
            */

            funcs.setError(null);
            funcs.setCurrent(null);
            $http.post('/scenarios/', jsyaml.safeDump(sc, {schema: scenario_schema}))
                .then(
                    function(response) { funcs.setCurrent(response.data.scenario_id); funcs.queryScenarios(); },
                    function(response) { funcs.setError(response.data); });
        };

        funcs.setError = function(s) {
            error = s;
        };

        funcs.getError = function(e) {
            return error;
        };

        funcs.setCurrent = function(scenario_id) {
            current_scenario_id = scenario_id;
        };

        funcs.getCurrent = function() {
            return current_scenario_id;
        };

        funcs.getScenarios = function() {
            return scenarios;
        };

        funcs.getStores = function() {
            return stores;
        };

        funcs.getStore = function(store_id) {
            for (var i=0; i<stores.length; i++) {
                if (stores[i].id == store_id) {
                    return stores[i];
                }
            }
            return null;
        };

        funcs.storeIsSuitable = function(store_id) {
            var store = funcs.getStore(store_id);
            return (store != null) ? store.suitable : false;
        };

        funcs.queryScenarios();
        funcs.queryGFStores();

        return funcs;
    })

    .controller('ScenarioListController', function($scope, $location, Scenarios) {
        $scope.scenarios = Scenarios;
        $scope.map_url = function(scenario_id) {
            return '/scenarios/' + scenario_id + '/map.png';
        }
        $scope.archive_url = function(scenario_id) {
            return '/scenarios/' + scenario_id + '/scenario.tar';
        }
    })

    .controller('ScenarioGenerator', function($scope, Scenarios) {

        $scope.input = {};
        $scope.input.radius = '1000';
        $scope.input.lat = '';
        $scope.input.lon = '';
        $scope.input.store_id = 'global_2s';
        $scope.input.ev_nevents = '1';
        $scope.input.ev_magnitude_min = '4';
        $scope.input.ev_magnitude_max = '6';
        $scope.input.ev_depth_min = '1.0';
        $scope.input.ev_depth_max = '20.0';
        $scope.input.ev_radius = '';
        $scope.input.st_nstations = '10';
        $scope.input.st_avoid_water = true;
        $scope.input.ns_white_noise_scale = '1e-6';
        var scenarios = Scenarios;

        $scope.get_scenario_error = Scenarios.getError;

        var form_value_update = function() {
            update_store_suitability();
        }

        $scope.error = {};
        $scope.$watch('input', form_value_update, true)

        var previous_params = null;

        var update_store_suitability = function() {
            var params = {
                s_lat: 0.0,
                s_lon: 0.0,
                s_radius: ($scope.input.ev_radius != '' ? $scope.input.ev_radius : $scope.input.radius) * 1000.,
                s_depth_min: $scope.input.ev_depth_min * 1000.,
                s_depth_max: $scope.input.ev_depth_max * 1000.,
                t_lat: 0.0,
                t_lon: 0.0,
                t_radius: $scope.input.radius * 1000.};

            if (!angular.equals(params, previous_params)) {
                Scenarios.querySuitableGFStores(params);
                previous_params = params;
            }
        };

        $scope.store_is_suitable = function(store_id) {
            return scenarios.storeIsSuitable(store_id);
        }

        $scope.get_store = function(store_id) {
            return scenarios.getStore(store_id);
        }

        $scope.get_stores = function() {
            return scenarios.getStores();
        };

        $scope.generate = function() {
            var data = {};
            $scope.error = {};

            parse_fields_float(
                ['radius', 'ev_radius', 'ev_depth_min', 'ev_depth_max'],
                $scope.input, data, $scope.error, 1000.);

            parse_fields_float(
                ['lat', 'lon', 'ev_magnitude_min', 'ev_magnitude_max', 'ns_white_noise_scale'],
                $scope.input, data, $scope.error, 1.0);

            parse_fields_int(
                ['ev_nevents', 'st_nstations'],
                $scope.input, data, $scope.error);

            if ($scope.error.length > 0) {
                return
            }

            data.ns_white_noise_scale = $scope.input.ns_white_noise_scale || false;
            data.st_avoid_water = $scope.input.st_avoid_water || false;
            data.store_id = $scope.input.store_id;

            var sc = new ScenarioGenerator({
                avoid_water: true,
                center_lat: data.lat,
                center_lon: data.lon,
                radius: data.radius,
                ntries: 20,
                source_generator: new DCSourceGenerator({
                    radius: data.ev_radius,
                    nevents: data.ev_nevents,
                    magnitude_min: data.ev_magnitude_min,
                    magnitude_max: data.ev_magnitude_max,
                    depth_min: data.ev_depth_min,
                    depth_max: data.ev_depth_max}),
                target_generators: [
                    new WaveformGenerator({
                        store_id: data.store_id,
                        seismogram_quantity: 'displacement',
                        fmin: 0.01,
                        station_generator: new RandomStationGenerator({
                            network_name: '',
                            ntries: 20,
                            nstations: data.st_nstations,
                            avoid_water: data.st_avoid_water}),
                        noise_generator: new WhiteNoiseGenerator({
                            scale: data.ns_white_noise_scale})
                    })
                ]
            });

            scenarios.generateScenario(sc);
        };
    })

    .filter('format_time', function() {
        return function(time) {
            return time.substr(0,10) + ' ' + time.substr(11,19);
        };
    });

