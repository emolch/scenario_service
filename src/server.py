import time
import random
import hashlib
import os
import signal
import os.path as op
import json
import socket
import logging
import threading
import asyncio
import traceback

from concurrent.futures import ProcessPoolExecutor

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.gen
import tornado.autoreload

# Needed for older tornado versions (appeared in v. 5.1.1?)
try:
    from tornado.platform.asyncio import AnyThreadEventLoopPolicy

    asyncio.set_event_loop_policy(AnyThreadEventLoopPolicy())
except ImportError:
    pass

from pyrocko import guts, util
from pyrocko.guts import String, Int

from scenario_service import __version__
from grond.meta import HasPaths, Path, GrondError
from pyrocko import scenario
from grond import EngineConfig

from pyrocko.gf import StringID

from . import common


logger = logging.getLogger('grond.scenario.server')

id_pattern = StringID.pattern[1:-1]


ext_to_mimetype = {
    'png': 'image/png',
    'pdf': 'application/pdf',
    'zip': 'application/zip',
    'tgz': 'application/x-compressed',
    'tar': 'application/x-tar'}


def get_argument_as(handler, name, default, type):
    try:
        return type(handler.get_argument(name, default))
    except (ValueError, TypeError):
        return default


def ehash(s):
    return hashlib.sha1(s.encode('utf8')).hexdigest()


class ServerError(scenario.ScenarioError):
    pass


class ServerConfig(HasPaths):
    cookie_secret_path = Path.T(default='config/server.cookie_secret')
    static_path = Path.T(optional=True)
    address = String.T(default='default')
    port = Int.T(default=8889)
    nworkers = Int.T(default=4)
    scenario_path = Path.T(default='scenarios')
    engine_config = EngineConfig.T(default=EngineConfig.D())

    def get_engine(self):
        return self.engine_config.get_engine()

    def get_scenario_collection(self):
        return scenario.ScenarioCollection(
            self.expand_path(self.scenario_path), self.get_engine())


class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, *args, **kwargs):
        tornado.web.RequestHandler.__init__(self, *args, **kwargs)
        self._error_message = None

    def initialize(self, pool=None, scenario_collection=None):
        self.pool = pool
        self.scenario_collection = scenario_collection

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def set_error(self, message):
        self._error_message = message

    def write_error(self, status_code, **kwargs):
        if self._error_message is not None:
            logger.error('request error: %i, %s' % (
                status_code, self._error_message))

            self.write(self._error_message)
            self._error_message = None
        else:
            tornado.web.RequestHandler.write_error(self, status_code, **kwargs)


def exc_wrap(func, *args, **kwargs):

    try:
        return func(*args, **kwargs)
    except Exception:
        traceback.print_exc()
        raise


def scenario_get_file(sc, base, fmt):
    if base == 'map':
        if fmt not in ['png', 'pdf']:
            raise scenario.ScenarioError(
                'Invalid map format: %s' % fmt)

        return sc.get_map(fmt)

    if (base, fmt) == ('scenario', 'tar'):
        return sc.get_archive()


restrictions = {
    scenario.ScenarioGenerator:
        dict(
            radius=dict(optional=False, ge=100., le=3000e3, unit='m'),
            target_generators=dict(
                len_eq=1, element_type_eq=scenario.WaveformGenerator),
            source_generator=dict(
                type_eq=scenario.DCSourceGenerator)),

    scenario.WaveformGenerator:
        dict(
            vmin_cut=dict(ge=1000., le=3000.),
            vmax_cut=dict(ge=4000., le=10000.)),

    scenario.StationGenerator:
        dict(
            nstations=dict(ge=1, le=100)),

    scenario.SourceGenerator:
        dict(
            nevents=dict(ge=1, le=100)),

    scenario.DCSourceGenerator:
        dict(
            magnitude_min=dict(ge=-2.0, le=10.0),
            magnitude_max=dict(optional=True, ge=-2.0, le=10.0),
            b_value=dict(optional=True, ge=0.1, le=10.)),

    scenario.LocationGenerator:
        dict(
            radius=dict(optional=True, ge=100., le=3000e3),
            ntries=dict(ge=1, le=100))}


condition_map = dict(
        le=(
            (lambda x, v: x <= v),
            (lambda v: 'must be less or equal to %g' % v)),
        ge=(
            (lambda x, v: x >= v),
            (lambda v: 'must be greater or equal to %g' % v)),
        lt=(
            (lambda x, v: x < v),
            (lambda v: 'must be less than %g' % v)),
        gt=(
            (lambda x, v: x > v),
            (lambda v: 'must be less than %g' % v)),
        element_type_eq=(
            (lambda x, v: all(type(e) == v for e in x)),
            (lambda v: 'elements must be of type %s' % v.__name__)),
        type_eq=(
            (lambda x, v: type(x) == v),
            (lambda v: 'must be of type %s' % v.__name__)),
        len_eq=(
            (lambda x, v: len(x) == v),
            (lambda v: 'must have %i elements' % v)))


def assert_condition(path, unit, x, condition, value):
    def to_str(path, unit):
        return guts.path_to_str(path) + (
            ' [%s]' % unit if unit is not None else '')

    fcond, fstr = condition_map[condition]
    try:
        if not fcond(x, value):
            raise ServerError(
                '%s: %s' % (to_str(path, unit), fstr(value)))

    except ServerError:
        raise

    except Exception as e:
        raise ServerError(
            '%s: check failed: %s' % (to_str(path, unit), str(e)))


def assert_conditions(path, x, conditions):
    conditions = conditions.copy()
    if conditions.pop('optional', False) and x is None:
        return

    unit = conditions.pop('unit', None)

    for cond in 'type_eq', 'element_type_eq':
        if cond in conditions:
            assert_condition(path, unit, x, cond, conditions.pop(cond))

    for cond, value in conditions.items():
        assert_condition(path, unit, x, cond, value)


class ScenarioHandler(BaseHandler):
    '''Handle scenario put, get, list events.'''

    processing = set()

    def check_restrictions(self, scenario_generator):
        def assert_(condition, fail):
            if not condition:
                raise ServerError(fail)

        sc = scenario_generator
        assert_(
            type(sc) == scenario.ScenarioGenerator,
            'Only ScenarioGenerator objects accepted.')

        for path, obj in guts.walk(sc):
            for typ, element_conditions in restrictions.items():
                if isinstance(obj, typ):
                    for element, conditions in element_conditions.items():
                        assert_conditions(
                            path + (element,),
                            getattr(obj, element),
                            conditions)

    def post(self, *args):
        try:
            scenario_generator = guts.load(
                string=self.request.body.decode('utf8'))

            if type(scenario_generator) != scenario.ScenarioGenerator:
                raise ServerError('Not a ScenarioGenerator object.')

            scenario_generator.validate()

            logger.info(
                'Scenario generator received:\n%s' % str(scenario_generator))

            self.check_restrictions(scenario_generator)

        except scenario.ScenarioError as e:
            logger.error(str(e))
            self.set_error(str(e))
            self.send_error(400)
            return

        except Exception as e:
            logger.error(str(e))
            self.set_error('invalid scenario request')
            self.send_error(400)
            return

        if scenario_generator.seed is None:
            scenario_generator.seed = random.randint(1, 2**32-1)

        try:
            scenario_generator.find_realisation(10)
            s = scenario_generator.dump()

            scenario_id = 'scenario_' + ehash(s)

            self.scenario_collection.add_scenario(
                scenario_id, scenario_generator)

            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps({'scenario_id': scenario_id}))

        except scenario.ScenarioError as e:
            logger.error(str(e))
            self.set_error(str(e))
            self.send_error(400)

    async def get_file(self, scenario_id, base, fmt):

        sc = self.scenario_collection.get_scenario(scenario_id)
        if not self.pool:
            path = exc_wrap(scenario_get_file, sc, base, fmt)

        else:
            pkey = (scenario_id, base)
            while pkey in self.processing:
                await tornado.gen.sleep(1.0)

            self.processing.add(pkey)
            try:
                path = await tornado.ioloop.IOLoop.current().run_in_executor(
                    self.pool, exc_wrap, scenario_get_file, sc, base, fmt)

            finally:
                self.processing.remove(pkey)

        return path

    async def get(self, *args):
        if len(args) not in (0, 2):
            self.send_error(404)
            return

        if len(args) == 0:
            self.set_header('Content-Type', 'application/json')

            scenarios = reversed(
                self.scenario_collection.list_scenarios(-10, None))

            self.write(json.dumps(guts.dump_all(scenarios)))
            return

        scenario_id, entry = args

        base, fmt = op.splitext(entry)
        fmt = fmt[1:]
        path = await self.get_file(scenario_id, base, fmt)

        f = open(path, 'rb')
        self.set_header('Content-Type', ext_to_mimetype[fmt])
        self.write(f.read())


class SuitableGFStoresHandler(BaseHandler):
    '''Handle geometry to gf store requests.'''

    def get(self):
        self.set_header('Content-Type', 'application/json')

        def get_arg(k):
            return get_argument_as(self, k, None, float)

        args = [get_arg(k) for k in [
            's_lat', 's_lon', 's_radius', 't_lat', 't_lon', 't_radius']]

        distance_range = scenario.distance_range(*args)

        d0, d1 = distance_range
        if d0 == 0.0:
            d0 = 4e3   # TODO: remove when stores have been
                       # been recomputed  # noqa

        distance_range = d0, d1

        source_depth_range = [
            get_arg(k) for k in ['s_depth_min', 's_depth_max']]

        if None in source_depth_range:
            store_ids = []

        else:
            store_ids = self.scenario_collection.get_suitable_store_ids(
                distance_range=distance_range,
                source_depth_range=source_depth_range)

        self.write(json.dumps(store_ids))


class GFStoresHandler(BaseHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        stores = self.scenario_collection.get_stores()
        data = []
        for store in stores:
            conf = store.config
            data.append(dict(
                id=conf.id,
                uuid=conf.uuid,
                sample_rate=conf.sample_rate,
                component_scheme=conf.component_scheme,
                short_type=conf.short_type,
                short_extent=conf.short_extent))

        self.write(json.dumps(data))


def get_cookie_secret(path):
    if not op.exists(path):
        util.ensuredirs(path)
        temp_umask = os.umask(0x77)
        with open(path, 'wb') as f:
            f.write(os.urandom(32))

        os.umask(temp_umask)

    with open(path, 'rb') as f:
        return f.read()


def read_config(path):
    config = guts.load(filename=path)
    if not isinstance(config, ServerConfig):
        raise GrondError('invalid server configuration in file "%s"' % path)

    config.set_basepath(op.dirname(path) or '.')
    return config


def write_config(config, path):
    try:
        basepath = config.get_basepath()
        dirname = op.dirname(path) or '.'
        config.change_basepath(dirname)
        guts.dump(
                config,
                filename=path,
                header='Configuration file for scenario_service, '
                       'version %s' % __version__)

        config.change_basepath(basepath)

    except OSError:
        raise GrondError(
                'Cannot write Grond configuration file: %s' % path)


def get_worker_pid(i):
    time.sleep(0.1)


g_terminate = False
g_error = False


def run(config, debug=False, open=False):
    nworkers = 4
    if nworkers == 0:
        pool = None
        worker_pids = []  # noqa
    else:
        # make worker processes deaf to keyboard interrupts
        orig = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = ProcessPoolExecutor(nworkers)
        # prelaunch workers to prevent all sorts of forking problems
        worker_pids = list(pool.map(get_worker_pid, range(nworkers)))  # noqa
        signal.signal(signal.SIGINT, orig)

    xp = config.expand_path

    if config.static_path is not None:
        static_path = xp(config.static_path)
    else:
        static_path = op.join(op.dirname(__file__), 'static')

    settings = dict(
        static_path=op.join(op.dirname(__file__), 'static'),
        debug=debug)

    app_settings = dict(
        static_path=static_path,
        debug=debug,
        cookie_secret=get_cookie_secret(xp(config.cookie_secret_path)))

    index_path = op.join(settings['static_path'], 'index.html')

    if config.address == 'default':
        address = [
            (s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close())
            for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]

    elif config.address == 'localhost':
        address = ''

    else:
        address = config.address

    url = 'http://%s:%s/' % (address or 'localhost', config.port)

    logger.info('Starting web service on %s' % url)

    stuff = {}

    if pool:
        tornado.autoreload.add_reload_hook(pool.shutdown)

    def serve():
        scenario_collection = config.get_scenario_collection()

        try:
            app = tornado.web.Application([
                (r'/()', tornado.web.StaticFileHandler,
                    dict(path=index_path)),
                (r'/scenarios/?', ScenarioHandler, dict(
                    pool=pool, scenario_collection=scenario_collection)),
                (r'/scenarios/(%s)/(map\.png|scenario\.tar)' % id_pattern,
                    ScenarioHandler, dict(
                        pool=pool, scenario_collection=scenario_collection)),
                (r'/gf_stores/suitable', SuitableGFStoresHandler,
                    dict(scenario_collection=scenario_collection)),
                (r'/gf_stores/list', GFStoresHandler,
                    dict(scenario_collection=scenario_collection)),
                # (r'/login', LoginHandler, dict(users=users)),
                # (r'/logout', LogoutHandler),
            ], **app_settings)

            stuff['httpserver'] = tornado.httpserver.HTTPServer(app)

            try:
                stuff['httpserver'].listen(config.port, address=address)
            except OSError as e:
                raise common.Error(str(e))

            stuff['instance'] = tornado.ioloop.IOLoop.current()
            stuff['instance'].start()

        except Exception as e:
            logger.error('Server process terminated with error: %s' % e)
            global g_terminate
            g_terminate = True
            global g_error
            g_error = e

        finally:
            if pool:
                pool.shutdown()

    def shutdown():
        maxwait = 2

        if 'httpserver' in stuff:
            stuff['httpserver'].stop()

        if 'instance' in stuff:
            instance = stuff['instance']
            deadline = time.time() + maxwait
            logger.info('Will terminate in max %i seconds...' % maxwait)

            def terminate():
                now = time.time()
                if now < deadline:  # and check for running stuff in event loop
                    instance.add_timeout(now + 1, terminate)
                else:
                    instance.stop()

            terminate()

    thread = threading.Thread(None, serve)
    thread.start()

    if open:
        import webbrowser
        if open:
            webbrowser.open(url)

    def handler(signum, frame):
        global g_terminate
        g_terminate = True

    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)

    while not g_terminate:
        time.sleep(0.1)

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGTERM, signal.SIG_DFL)

    if 'instance' in stuff:
        logger.info('Stopping web service...')
        stuff['instance'].add_callback(shutdown)

    thread.join()
    if 'instance' in stuff:
        logger.info('Stopping web service done.')

    if g_error:
        raise common.Error(g_error)
