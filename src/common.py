from __future__ import print_function, absolute_import


class Error(Exception):
    '''Base class for errors in scenario_service.'''
