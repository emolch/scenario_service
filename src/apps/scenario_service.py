from __future__ import print_function, absolute_import

import os.path as op
import sys
import logging
import scenario_service
from io import StringIO

from optparse import OptionParser

from pyrocko import util


logger = logging.getLogger('scenario_service.main')


def d2u(d):
    if isinstance(d, dict):
        return dict((k.replace('-', '_'), v) for (k, v) in d.items())
    else:
        return d.replace('-', '_')


subcommand_descriptions = {
    'run': 'run scenario_service management server',
    'version': 'print version number',
}

subcommand_usages = {
    'run': 'run <configfile> [options]',
    'version': 'version [options]',
}

subcommands = subcommand_descriptions.keys()

program_name = 'scenario_service'

usage_tdata = d2u(subcommand_descriptions)
usage_tdata['program_name'] = program_name
usage_tdata['version_number'] = scenario_service.__version__


usage = '''%(program_name)s <subcommand> [options] [--] <arguments> ...

scenario_service: Web service to create synthetic seismo-geodetic datasets.

This is scenario_service version %(version_number)s.

Subcommands:

    run             %(run)s
    version         %(version)s

To get further help and a list of available options for any subcommand run:

    %(program_name)s <subcommand> --help

''' % usage_tdata


def main(args=None):
    if not args:
        args = sys.argv

    args = list(args)
    if len(args) < 2:
        sys.exit('Usage: %s' % usage)

    args.pop(0)
    command = args.pop(0)

    if command in subcommands:
        globals()['command_' + d2u(command)](args)

    elif command in ('--help', '-h', 'help'):
        if command == 'help' and args:
            acommand = args[0]
            if acommand in subcommands:
                globals()['command_' + acommand](['--help'])

        sys.exit('Usage: %s' % usage)

    else:
        die('No such subcommand: %s' % command)


def add_common_options(parser):
    parser.add_option(
        '--loglevel',
        action='store',
        dest='loglevel',
        type='choice',
        choices=('critical', 'error', 'warning', 'info', 'debug'),
        default='info',
        help='set logger level to '
             '"critical", "error", "warning", "info", or "debug". '
             'Default is "%default".')


def process_common_options(command, parser, options):
    util.setup_logging(program_name, options.loglevel)


def cl_parse(command, args, setup=None, details=None):
    usage = subcommand_usages[command]
    descr = subcommand_descriptions[command]

    if isinstance(usage, str):
        usage = [usage]

    susage = '%s %s' % (program_name, usage[0])
    for s in usage[1:]:
        susage += '\n%s%s %s' % (' '*7, program_name, s)

    description = descr[0].upper() + descr[1:] + '.'

    if details:
        description = description + '\n\n%s' % details

    parser = OptionParser(usage=susage, description=description)

    if setup:
        setup(parser)

    add_common_options(parser)
    (options, args) = parser.parse_args(args)
    process_common_options(command, parser, options)
    return parser, options, args


def die(message, err='', prelude=''):
    if prelude:
        prelude = prelude + '\n'

    if err:
        err = '\n' + err

    sys.exit('%s%s failed: %s%s' % (prelude, program_name, message, err))


def help_and_die(parser, message):
    sio = StringIO()
    parser.print_help(sio)
    die(message, prelude=sio.getvalue())


def command_run(args):

    def setup(parser):
        parser.add_option(
            '--init',
            dest='initialize',
            action='store_true',
            default=False,
            help='initialize service directory')

        parser.add_option(
            '--debug',
            dest='debug',
            action='store_true',
            default=False,
            help='run service in debug mode')

        parser.add_option(
            '--open',
            dest='open',
            action='store_true',
            default=False,
            help='open in web browser')

    parser, options, args = cl_parse('run', args, setup=setup)

    if len(args) != 1:
        help_and_die(parser, 'missing arguments')

    from scenario_service import server

    if options.initialize:
        service_path, = args
        if op.exists(service_path):
            help_and_die(parser, 'path already exists')

        util.ensuredir(service_path)

        config = server.ServerConfig()
        config.set_basepath(op.join(service_path))
        config_path = op.join(service_path, 'config', 'server.config')
        util.ensuredirs(config_path)
        server.write_config(config, config_path)
        return

    config_path, = args
    config = server.read_config(config_path)

    server.run(config, debug=options.debug, open=options.open)


def command_version(args):
    def setup(parser):
        pass

    parser, options, args = cl_parse('version', args, setup)

    print(scenario_service.__version__)
    return


if __name__ == '__main__':
    main()
