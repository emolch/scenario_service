#!/usr/bin/env python

from __future__ import print_function, absolute_import

import os
import time

from setuptools import setup
from setuptools.command.install import install
from setuptools.command.build_py import build_py


version = '0.1'


class CustomInstallCommand(install):
    def run(self):
        install.run(self)


class CustomBuildPyCommand(build_py):

    def make_info_module(self, packname, version):
        '''
        Put version and install-time information into file src/setup_info.py.
        '''

        datestr = time.strftime('%Y-%m-%d_%H:%M:%S')
        module_code = '''# This module is automatically created from setup.py
version = %s
installed_date = %s
''' % tuple([repr(x) for x in (
            version, datestr)])

        outfile = self.get_module_outfile(
            self.build_lib, ['scenario_service'], 'setup_info')

        dir = os.path.dirname(outfile)
        self.mkpath(dir)
        with open(outfile, 'w') as f:
            f.write(module_code)

    def run(self):
        self.make_info_module('scenario_service', version)
        build_py.run(self)


def p(*args):
    return '.'.join(args)


package_name = 'scenario_service'
setup(
    name=package_name,

    cmdclass={
        'install': CustomInstallCommand,
        'build_py': CustomBuildPyCommand,
    },

    version=version,

    author='The Pyrocko Developers',

    packages=[
        p(package_name),
        p(package_name, 'apps')],

    package_dir={package_name: 'src'},

    package_data={package_name: [
        'static/*.html',
        'static/css/*.css',
        'static/js/*.js',
        'static/fonts/*',
        'static/images/*']},

    entry_points={
        'console_scripts': [
            'scenario_service = scenario_service.apps.scenario_service:main',
        ]
    })
